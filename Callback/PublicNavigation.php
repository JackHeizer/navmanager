<?php

class NavManager_Callback_PublicNavigation
{	
	public static function canSearch(array &$navTab)
	{
		$visitor = XenForo_Visitor::getInstance();
		$navTab['canSearch'] = $visitor->canSearch();
	}
}