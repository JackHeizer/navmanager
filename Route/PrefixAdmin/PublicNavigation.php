<?php

/**
 * Route prefix handler for public navigation in the admin control panel.
 *
 * @package NavManager_PublicNavigation
 */
class NavManager_Route_PrefixAdmin_PublicNavigation implements XenForo_Route_Interface
{
	/**
	 * Match a specific route for an already matched prefix.
	 *
	 * @see XenForo_Route_Interface::match()
	 */
	public function match($routePath, Zend_Controller_Request_Http $request, XenForo_Router $router)
	{
		$action = $router->resolveActionWithStringParam($routePath, $request, 'navigation_id');
		return $router->getRouteMatch('NavManager_ControllerAdmin_PublicNavigation', $action, 'publicNavigation');
	}

	/**
	 * Method to build a link to the specified page/action with the provided
	 * data and params.
	 *
	 * @see XenForo_Route_BuilderInterface
	 */
	public function buildLink($originalPrefix, $outputPrefix, $action, $extension, $data, array &$extraParams)
	{
		return XenForo_Link::buildBasicLinkWithStringParam($outputPrefix, $action, $extension, $data, 'navigation_id');
	}
}