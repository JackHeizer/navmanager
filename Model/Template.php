<?php

class NavManager_Model_Template extends XFCP_NavManager_Model_Template
{
	/**
	 * Renames a template. If you get a conflict, it will
	 * be silently ignored.
	 *
	 * @param string $oldName
	 * @param string $newName
	 */
	public function renameMasterTemplate($oldName, $newName)
	{
		$template = $this->getTemplateInStyleByTitle($oldName);
		if (!$template)
		{
			return;
		}
		
		$dw = XenForo_DataWriter::create('XenForo_DataWriter_Template', XenForo_DataWriter::ERROR_SILENT);
		$dw->setExistingData($template, true);
		$dw->set('title', $newName);
		$dw->save();
	}
	
	/**
	 * Inserts or updates a master template. Errors will be silently ignored.
	 *
	 * @param string $title
	 * @param string $templateText
	 * @param string $addOnId
	 */
	public function insertOrUpdateMasterTemplate($title, $templateText, $addOnId)
	{
		$template = $this->getTemplateInStyleByTitle($title);

		$dw = XenForo_DataWriter::create('XenForo_DataWriter_Template', XenForo_DataWriter::ERROR_SILENT);
		if ($template)
		{
			$dw->setExistingData($template, true);
		}
		$dw->set('title', $title);
		$dw->set('template', $templateText);
		$dw->set('style_id', 0);
		$dw->set('addon_id', $addOnId);
		$dw->save();
	}
	
	/**
	 * Deletes the named master template if it exists.
	 *
	 * @param string $title
	 */
	public function deleteMasterTemplate($title)
	{
		$template = $this->getTemplateInStyleByTitle($title);
		if (!$template)
		{
			return;
		}

		$dw = XenForo_DataWriter::create('XenForo_DataWriter_Template', XenForo_DataWriter::ERROR_SILENT);
		$dw->setExistingData($template, true);
		$dw->delete();
	}
}