<?php

/**
 * Model for public navigation.
 *
 * @package NavManager_PublicNavigation
 */
class NavManager_Model_PublicNavigation extends XenForo_Model
{
	/**
	 * Gets all public navigation entries, in parent-display order.
	 *
	 * @param array Fetch options
	 *
	 * @return array [navigation id] => info
	 */
	public function getPublicNavigationEntries()
	{
		return $this->fetchAllKeyed('
			SELECT public_navigation.*
			FROM ap_public_navigation AS public_navigation
			ORDER BY
				public_navigation.parent_navigation_id,
				public_navigation.display_order
		', 'navigation_id');
	}

	/**
	 * Gets all public navigation entries with the specified parent, in display order.
	 *
	 * @param string $parentId
	 * @param array Fetch options
	 *
	 * @return array [navigation id] => info
	 */
	public function getPublicNavigationEntriesWithParent($parentId)
	{
		return $this->fetchAllKeyed('
			SELECT public_navigation.*
			FROM ap_public_navigation AS public_navigation
			WHERE
				public_navigation.parent_navigation_id = ?
			ORDER BY
				public_navigation.display_order
		', 'navigation_id', $parentId);
	}

	/**
	 * Gets the specified public navigation entry.
	 *
	 * @param string $navigationId
	 * @param array Fetch options
	 *
	 * @return array|false
	 */
	public function getPublicNavigationEntryById($navigationId)
	{
		return $this->_getDb()->fetchRow('
			SELECT public_navigation.*
			FROM ap_public_navigation AS public_navigation
			WHERE
				public_navigation.navigation_id = ?
		', $navigationId);
	}

	/**
	 * Gets the specified public navigation entries.
	 *
	 * @param array $navigationIds
	 * @param array Fetch options
	 *
	 * @return array [navigation id] => info
	 */
	public function getPublicNavigationEntriesByIds(array $navigationIds)
	{
		if (!$navigationIds)
		{
			return array();
		}
	
		return $this->fetchAllKeyed('
			SELECT public_navigation.*
			FROM ap_public_navigation AS public_navigation
			WHERE
				public_navigation.navigation_id IN (' . $this->_getDb()->quote($navigationIds) . ')
		', 'navigation_id');
	}

	/**
	 * Group public navigation entries by their parent.
	 *
	 * @param array $navigation List of navigation entries to group
	 *
	 * @return array [parent navigation id][navigation id] => info
	 */
	public function groupPublicNavigation(array $navigation)
	{
		$output = array(
			'' => array()
		);
		foreach ($navigation AS $nav)
		{
			$output[$nav['parent_navigation_id']][$nav['navigation_id']] = $nav;
		}

		return $output;
	}

	/**
	 * Get the public navigation list in the correct display order. This can be processed
	 * linearly with depth markers to visually represent the tree.
	 *
	 * @param array|null $navigation Navigation entries; if null, grabbed automatically
	 * @param string $root Root node to traverse from
	 * @param integer $depth Depth to start at
	 *
	 * @return array [navigation id] => info, with depth key set
	 */
	public function getPublicNavigationInOrder(array $navigation = null, $root = '', $depth = 0)
	{
		if (!is_array($navigation))
		{
			$navigation = $this->groupPublicNavigation($this->getPublicNavigationEntries());
		}

		if (!isset($navigation[$root]))
		{
			return array();
		}

		$output = array();
		foreach ($navigation[$root] AS $nav)
		{
			$nav['depth'] = $depth;
			$output[$nav['navigation_id']] = $nav;

			$output += $this->getPublicNavigationInOrder($navigation, $nav['navigation_id'], $depth + 1);
		}

		return $output;
	}

	/**
	 * Gets the public navigation as simple options for use in a select. Uses depth markers for tree display.
	 *
	 * @param array|null $navigation Navigation entries; if null, grabbed automatically
	 * @param string $root Root node to traverse from
	 * @param integer $depth Depth to start at
	 *
	 * @return array [] => [value, label, depth]
	 */
	public function getPublicNavigationOptions(array $navigation = null, $root = '', $depth = 0)
	{
		$navList = $this->preparePublicNavigationEntries(
			$this->getPublicNavigationInOrder($navigation, $root, $depth)
		);

		$options = array();
		foreach ($navList AS $nav)
		{
			if($nav['depth'] < 1)
				$options[] = array(
					'value' => $nav['navigation_id'],
					'label' => $nav['title'],
					'depth' => ++$nav['depth']
				);
		}

		return $options;
	}

	/**
	 * Determines if the specified parent navigation ID is a valid parent for the navigation ID.
	 *
	 * @param string $potentialParentId
	 * @param string $navigationId
	 * @param array|null $navigation Navigation entries; if null, grabbed automatically
	 *
	 * @return boolean
	 */
	public function isPublicNavigationEntryValidParent($potentialParentId, $navigationId, array $navigation = null)
	{
		if ($potentialParentId == $navigationId)
		{
			return false;
		}
		else if ($potentialParentId === '')
		{
			return true;
		}

		if (!is_array($navigation))
		{
			$navigation = $this->getPublicNavigationEntries();
		}

		if (!isset($navigation[$potentialParentId]))
		{
			return false;
		}
		else if (!isset($navigation[$navigationId]))
		{
			return true;
		}

		$walkId = $potentialParentId;
		do
		{
			$walkNav = $navigation[$walkId];
			if ($walkNav['navigation_id'] == $navigationId)
			{
				return false;
			}

			$walkId = $walkNav['parent_navigation_id'];
		}
		while (isset($navigation[$walkId]));
		
		foreach ($navigation AS $navKey => $navInfo)
		{
			if($navInfo['parent_navigation_id'] == $navigationId)
			{
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Gets all of the navigation nodes as a list of parents nodes, each with
	 * a list of child nodes if it has any.
	 * 
	 * This can probably be cleaned up quite a bit, but works for now
	 * 
	 * @return array
	 */
	public function preparePublicNavigationForDisplay()
	{
		$entries = $this->getPublicNavigationEntries();
		$groupedEntries = array();
		
		// Parents first
		foreach ($entries AS $entryKey => $entryValue)
		{
			if($entryValue['parent_navigation_id'] == '')
			{
				$groupedEntries[$entryKey] = $entryValue;
				$title = new XenForo_Phrase($this->getPublicNavigationPhraseName($entryKey));
				$href = ($entryValue['intern']) ? XenForo_Link::buildPublicLink(($entryValue['full']) ? 'full:' . $entryValue['link'] : $entryValue['link']) : $entryValue['link'];
				$groupedEntries[$entryKey]['title'] = $title;
				$groupedEntries[$entryKey]['href'] = $href;
				$groupedEntries[$entryKey]['position'] = 'home';
			}
		}
		
		// Then we add the children
		foreach ($entries AS $entryKey => $entryValue)
		{
			if($entryValue['parent_navigation_id'] != '')
			{
				if(!isset($groupedEntries[$entryValue['parent_navigation_id']]['linksTemplate']))
				{
					$groupedEntries[$entryValue['parent_navigation_id']]['linksTemplate'] = 
					$this->getPublicNavigationTemplateName($entryValue['parent_navigation_id']);
				}
				$groupedEntries[$entryValue['parent_navigation_id']]['children'][$entryKey] = $entryValue;
			}
		}
		
		return $groupedEntries;
	}

	/**
	 * Prepares a public navigation entry for display.
	 *
	 * @param array $entry
	 *
	 * @return array
	 */
	public function preparePublicNavigationEntry(array $entry)
	{
		$entry['title'] = new XenForo_Phrase($this->getPublicNavigationPhraseName($entry['navigation_id']));

		return $entry;
	}

	/**
	 * Prepares a list of public navigation entries for display.
	 *
	 * @param array $entries
	 *
	 * @return array
	 */
	public function preparePublicNavigationEntries(array $entries)
	{
		foreach ($entries AS &$entry)
		{
			$entry = $this->preparePublicNavigationEntry($entry);
		}

		return $entries;
	}

	/**
	 * Gets the name of an public navigation phrase.
	 *
	 * @param string $navigationId
	 *
	 * @return string
	 */
	public function getPublicNavigationPhraseName($navigationId)
	{
		// return 'public_navigation_' . $navigationId;
		return 'navmanager_' . $navigationId;
	}

	/**
	 * Gets the master title phrase value for the specified public navigation entry.
	 *
	 * @param string $navigationId
	 *
	 * @return string
	 */
	public function getPublicNavigationMasterTitlePhraseValue($navigationId)
	{
		$phraseName = $this->getPublicNavigationPhraseName($navigationId);
		return $this->_getPhraseModel()->getMasterPhraseValue($phraseName);
	}
	
	/**
	 * Gets the master template name for the specified public navigation entry.
	 * This will return a result even if an entry isn't supposed to have a template!
	 * 
	 * @param string $navigationId
	 * 
	 * @return string
	 */
	public function getPublicNavigationTemplateName($navigationId)
	{
		return 'navmanager_linktemplate_' . $navigationId;
	}
	
	/**
	 * Creates the links template for a parent node.
	 * 
	 * @param string $parentNavigationId
	 * 
	 * @return string the template text
	 */
	public function getLinksTemplate($parentNavigationId)
	{
		$entries = $this->getPublicNavigationEntriesWithParent($parentNavigationId);
		if(empty($entries))
			return null;
		$meta_links_template = XenForo_Application::get('options')->meta_links_template;
		$meta_link_template = XenForo_Application::get('options')->meta_link_template;
		$meta_perm_link_template = XenForo_Application::get('options')->meta_perm_link_template;
		$links = '';
		foreach ($entries AS $entryKey => $entryVal)
		{
			$link = ($entryVal['intern']) ? $this->_makeXenLink($entryVal['link'], $entryVal['data'], $entryVal['extra_params'], $entryVal['full']) : $entryVal['link'];
			$title = '{xen:phrase ' . $this->getPublicNavigationPhraseName($entryKey) . '}'; 
			$overlay = ($entryVal['overlay']) ? 'class="OverlayTrigger"' : '';
			$templateLinks = str_replace(array('{$link}','{$title}', '{$overlay}'), array($link, $title, $overlay), $meta_link_template);
			if(!empty($entryVal['permission_id']) && !empty($entryVal['permission_group_id']))
			{
				$templateLinks = str_replace(array('{$perm}', '{$permLink}'), array($this->_makePermission($entryVal['permission_id'], $entryVal['permission_group_id']), $templateLinks), $meta_perm_link_template); 
			}
			$links .= $templateLinks;
		}
		$linksTemplate = str_replace('{$links}', $links, $meta_links_template);
		return $linksTemplate;
	}
	
	/**
	 * Builds a {xen:link} construct for our template
	 * 
	 * @param string $link prefix route/action
	 * @param string $data extra data?
	 * @param string $extraParams key1=value1,key2=value2,... pairs in string form
	 */
	protected function _makeXenLink($link, $data, $extraParams, $full = false)
	{
		$xenLink = '{xen:link \'' . (($full) ? 'full:' . $link : $link) . '\'';
		if(!empty($data))
		{
			$xenLink .= ', \'' . $data . '\'';
		}
		if(!empty($extraParams) && !empty($data))
		{
			$xenLink .= ', \'' . $extraParams . '\'';
		}
		if(!empty($extraParams) && empty($data))
		{
			$xenLink .= ', \'\', \'' . $extraParams . '\'';
		}
		$xenLink .= '}';
		return $xenLink;
	}
	
	/**
	 * Builds a permission construct for our template. Everyone
	 * pulls from $visitor.permissions so we need to inject our
	 * "special" permissions with a hook later
	 * 
	 * @param string $permissionId
	 * @param string $permissionGroupId
	 */
	protected function _makePermission($permissionId, $permissionGroupId)
	{
		if($permissionGroupId == 'nmSpecialPermissions')
		{
			switch($permissionId)
			{
				case 'loggedIn':
					$permission = '{$visitor.user_id}';
					break;
				case 'canSearch':
					$permission = '{$canSearch}';
					break;
				case 'canUploadAvatar':
					$permission = '{$canUploadAvatar}';
					break;
				case 'canEditSignature':
					$permission = '{$canEditSignature}';
					break;
				case 'canUpdateStatus':
					$permission = '{$canUpdateStatus}';
					break;
			}
		}
		else 
		{
			$permission = '{$visitor.permissions.' . $permissionGroupId . '.' . $permissionId . '}';
		}
		return $permission;
	}
	
	/**
	 * Imports selected nodes from an XML file
	 * 
	 * @param string $fileName
	 * @param boolean $overwrite
	 * @throws XenForo_Exception
	 */
	public function importPublicNavigationNodeXML($fileName, $overwrite)
	{
		if (!file_exists($fileName) || !is_readable($fileName))
		{
			throw new XenForo_Exception(new XenForo_Phrase('please_enter_valid_file_name_requested_file_not_read'), true);
		}
		
		try
		{
			$document = new SimpleXMLElement($fileName, 0, true);
		}
		catch (Exception $e)
		{
			throw new XenForo_Exception(
				new XenForo_Phrase('provided_file_was_not_valid_xml_file'), true
			);
		}
		
		if ($document->getName() != 'public_navigation_nodes')
		{
			throw new XenForo_Exception(new XenForo_Phrase('invalid_public_navigation_xml'), true);
		}
		
		$entries = array();
		
		foreach ($document->node AS $node)
		{
			$entries[(string)$node['navigation_id']] = array();
			$this->_consumePublicNavigationNodeXML($node, &$entries[(string)$node['navigation_id']]);
		}

		$db = $this->_getDb();
		XenForo_Db::beginTransaction($db);
		
		$currentEntries = $this->getPublicNavigationEntries();
		
		// We need to insert root nodes first
		foreach ($entries AS $entryKey => $entryValue)
		{
			if(!empty($entryValue['parent_navigation_id']))
				continue;
			$this->_writePublicNavigationNode($entryKey, $entryValue, $currentEntries, $overwrite);
		}
		
		
		// Then we can (try to) insert child nodes
		foreach ($entries AS $entryKey => $entryValue)
		{
			if(empty($entryValue['parent_navigation_id']))
				continue;
			// If the parent doesn't exist, don't put it in the database
			// Some bits of code probably can't handle orphans well
			if(!isset($currentEntires[$entryValue['parent_navigation_id']]) && !isset($entries[$entryValue['parent_navigation_id']]))
				continue;
			$this->_writePublicNavigationNode($entryKey, $entryValue, $currentEntries, $overwrite);
		}
		
		XenForo_Db::commit($db);
	}
	
	/**
	 * Handles parsing each individual navigation node
	 * 
	 * @param SimpleXMLElement $xml
	 * @param array $entry
	 */
	protected function _consumePublicNavigationNodeXML(SimpleXMLElement $xml, array &$entry)
	{
		$entry['navigation_id'] = (string)$xml['navigation_id'];
		$entry['title'] = (string)$xml->title[0];
		$entry['link'] = (string)$xml->link[0];
		$entry['data'] = (string)$xml->data[0];
		$entry['extra_params'] = (string)$xml->extra_params[0];
		$entry['intern'] = (int)$xml->intern[0];
		$entry['overlay'] = (int)$xml->overlay[0];
		$entry['full'] = (int)$xml->full[0];
		$entry['parent_navigation_id'] = (string)$xml->parent_navigation_id[0];
		$entry['display_order'] = (int)$xml->display_order[0];
		$entry['perm'] = (string)$xml->perm[0];
		$entry['callback_class'] = (string)$xml->callback_class[0];
		$entry['callback_method'] = (string)$xml->callback_method[0];
		$entry['permission_id'] = (string)$xml->permission_id[0];
		$entry['permission_group_id'] = (string)$xml->permission_group_id[0];
	}
	
	/**
	 * Writes a given node to the database
	 * 
	 * @param string $entryKey
	 * @param array $entryValue
	 * @param array $currentEntries
	 * @param boolean $overwrite
	 */
	protected function _writePublicNavigationNode($entryKey, array $entryValue, array $currentEntries, $overwrite = false)
	{
		$dw = XenForo_DataWriter::create('NavManager_DataWriter_PublicNavigation');
		if(isset($currentEntries[$entryKey]))
		{
			if($overwrite)
			{
				$data = $this->getPublicNavigationEntryById($entryKey);
				$dw->setExistingData($data, true);
			}
			else
				return;
		}
		$dw->bulkSet(array(
			'navigation_id'         => $entryValue['navigation_id'],
			'parent_navigation_id'  => $entryValue['parent_navigation_id'],
			'display_order'         => $entryValue['display_order'],
			'link'                  => $entryValue['link'],
			'data'					=> $entryValue['data'],
			'extra_params'			=> $entryValue['extra_params'],
			'overlay'				=> $entryValue['overlay'],
			'full'					=> $entryValue['full'],
			'intern'				=> $entryValue['intern'],
			'callback_class'		=> $entryValue['callback_class'],
			'callback_method'		=> $entryValue['callback_method'],
			'permission_id'			=> $entryValue['permission_id'],
			'permission_group_id'	=> $entryValue['permission_group_id']
		));
		$dw->setExtraData(NavManager_DataWriter_PublicNavigation::DATA_TITLE, $entryValue['title']);
		$dw->save();		
	}
	
	/**
	 * Get the XML for a node
	 * 
	 * @param string $navigationId
	 * 
	 * @return DOMDocument
	 */
	public function getPublicNavigationNodeXML($navigationId)
	{
		return $this->getPublicNavigationNodesXML(array($navigationId));
	}
	
	/**
	 * Get the XML for an array of nodes
	 * 
	 * @param array $navigationIds
	 */
	public function getPublicNavigationNodesXML(array $navigationIds)
	{
		$document = new DOMDocument('1.0', 'utf-8');
		$document->formatOutput = true;
		$rootNode = $document->createElement('public_navigation_nodes');
		$document->appendChild($rootNode);
		
		foreach ($navigationIds AS $navigationId)
			$this->_appendPublicNavigationNodeXML($rootNode, $navigationId);
		
		return $document;
	}
	
	/**
	 * Appends a single XML node definition onto a larger XML list
	 * 
	 * @param DOMElement $rootNode
	 * @param string $navigationId
	 */
	protected function _appendPublicNavigationNodeXML(DOMElement $rootNode, $navigationId)
	{
		$entry = $this->getPublicNavigationEntryById($navigationId);
		
		$document = $rootNode->ownerDocument;
		$node = $document->createElement('node');
		$node->setAttribute('navigation_id', $navigationId);
		
		$node->appendChild($document->createElement('title', $this->getPublicNavigationMasterTitlePhraseValue($navigationId)));
		$node->appendChild($document->createElement('link', $entry['link']));
		$node->appendChild($document->createElement('data', $entry['data']));
		$node->appendChild($document->createElement('extra_params', $entry['extra_params']));
		$node->appendChild($document->createElement('intern', $entry['intern']));
		$node->appendChild($document->createElement('overlay', $entry['overlay']));
		$node->appendChild($document->createElement('full', $entry['full']));
		$node->appendChild($document->createElement('parent_navigation_id', $entry['parent_navigation_id']));
		$node->appendChild($document->createElement('display_order', $entry['display_order']));
		$node->appendChild($document->createElement('callback_class', $entry['callback_class']));
		$node->appendChild($document->createElement('callback_method', $entry['callback_method']));
		$node->appendChild($document->createElement('permission_id', $entry['permission_id']));
		$node->appendChild($document->createElement('permission_group_id', $entry['permission_group_id']));
		
		$rootNode->appendChild($node);
	}
	
	

	/**
	 * @return XenForo_Model_Phrase
	 */
	protected function _getPhraseModel()
	{
		return $this->getModelFromCache('XenForo_Model_Phrase');
	}
	
	/**
	 * @return XenForo_Model_Template
	 */
	protected function _getTemplateModel()
	{
		return $this->getModelFromCache('XenForo_Model_Template');
	}
}