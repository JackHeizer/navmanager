<?php

class NavManager_Model_Phrase extends XFCP_NavManager_Model_Phrase
{
	/**
	 * Inserts or updates a master (language 0) phrase and makes it cached. 
	 * Errors will be silently ignored.
	 *
	 * @param string $title
	 * @param string $text
	 * @param string $addOnId
	 */
	public function insertOrUpdateMasterPhraseCached($title, $text, $addOnId)
	{
		$phrase = $this->getPhraseInLanguageByTitle($title, 0);

		$dw = XenForo_DataWriter::create('XenForo_DataWriter_Phrase', XenForo_DataWriter::ERROR_SILENT);
		if ($phrase)
		{
			$dw->setExistingData($phrase, true);
		}
		else
		{
			$dw->set('language_id', 0);
		}
		$dw->set('title', $title);
		$dw->set('phrase_text', $text);
		$dw->set('addon_id', $addOnId);
		$dw->set('global_cache', true);
		if ($dw->isChanged('title') || $dw->isChanged('phrase_text'))
		{
			$dw->updateVersionId();
		}
		$dw->save();
	}
}