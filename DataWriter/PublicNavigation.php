<?php

/**
* Data writer for navigation.
*
* @package NavManager_PublicNavigation
*/
class NavManager_DataWriter_PublicNavigation extends XenForo_DataWriter
{
	/**
	 * Constant for extra data that holds the value for the phrase
	 * that is the title of this navigation entry.
	 *
	 * This value is required on inserts.
	 *
	 * @var string
	 */
	const DATA_TITLE = 'phraseTitle';

	/**
	 * Title of the phrase that will be created when a call to set the
	 * existing data fails (when the data doesn't exist).
	 *
	 * @var string
	 */
	protected $_existingDataErrorPhrase = 'requested_public_navigation_entry_not_found';

	/**
	* Gets the fields that are defined for the table. See parent for explanation.
	*
	* @return array
	*/
	protected function _getFields()
	{
		return array(
			'ap_public_navigation' => array(
				'navigation_id'         => array('type' => self::TYPE_STRING, 'maxLength' => 25, 'required' => true,
						'verification' => array('$this', '_verifyNavigationId'),
						'requiredError' => 'please_enter_valid_public_navigation_id'
				),
				'parent_navigation_id'  => array('type' => self::TYPE_STRING, 'maxLength' => 25, 'default' => ''),
				'display_order'         => array('type' => self::TYPE_UINT,   'default' => 10),
				'link'                  => array('type' => self::TYPE_STRING, 'maxLength' => 150, 'default' => ''),
				'data'					=> array('type' => self::TYPE_STRING, 'maxLength' => 150, 'default' => ''),
				'extra_params'			=> array('type' => self::TYPE_STRING, 'maxLength' => 150, 'default' => ''),
				'overlay'				=> array('type' => self::TYPE_BOOLEAN, 'default' => 0),
				'full'					=> array('type' => self::TYPE_BOOLEAN, 'default' => 0),
				'intern'				=> array('type' => self::TYPE_BOOLEAN, 'default' => 1),
				'callback_class'		=> array('type' => self::TYPE_STRING, 'maxLength' => 75, 'default' => ''),
				'callback_method'		=> array('type' => self::TYPE_STRING, 'maxLength' => 75, 'default' => ''),
				'permission_id'			=> array('type' => self::TYPE_STRING, 'maxLength' => 25, 'default' => ''),
				'permission_group_id' 	=> array('type' => self::TYPE_STRING, 'maxLength' => 25, 'default' => '')
			)
		);
	}

	/**
	* Gets the actual existing data out of data that was passed in. See parent for explanation.
	*
	* @param mixed
	*
	* @return array|false
	*/
	protected function _getExistingData($data)
	{
		if (!$id = $this->_getExistingPrimaryKey($data, 'navigation_id'))
		{
			return false;
		}

		return array('ap_public_navigation' => $this->_getPublicNavigationModel()->getPublicNavigationEntryById($id));
	}

	/**
	* Gets SQL condition to update the existing record.
	*
	* @return string
	*/
	protected function _getUpdateCondition($tableName)
	{
		return 'navigation_id = ' . $this->_db->quote($this->getExisting('navigation_id'));
	}

	/**
	 * Verifies that the navigation ID is valid.
	 *
	 * @param string $newId
	 *
	 * @return boolean
	 */
	protected function _verifyNavigationId($newId)
	{
		if (preg_match('/[^a-zA-Z0-9_]/', $newId))
		{
			$this->error(new XenForo_Phrase('please_enter_an_id_using_only_alphanumeric'), 'navigation_id');
			return false;
		}

		if ($this->isInsert() || $newId != $this->getExisting('navigation_id'))
		{
			$newIdConflict = $this->_getPublicNavigationModel()->getPublicNavigationEntryById($newId);
			if ($newIdConflict)
			{
				$this->error(new XenForo_Phrase('public_navigation_ids_must_be_unique'), 'navigation_id');
				return false;
			}
		}

		return true;
	}

	/**
	 * Pre-save handling.
	 */
	protected function _preSave()
	{
		$titlePhrase = $this->getExtraData(self::DATA_TITLE);
		if ($titlePhrase !== null && strlen($titlePhrase) == 0)
		{
			$this->error(new XenForo_Phrase('please_enter_valid_title'), 'title');
		}

		if ($this->isUpdate() && $this->isChanged('parent_navigation_id'))
		{
			if (!$this->_getPublicNavigationModel()->isPublicNavigationEntryValidParent(
				$this->get('parent_navigation_id'), $this->getExisting('navigation_id')
			))
			{
				$this->error(new XenForo_Phrase('please_select_valid_parent_public_navigation_entry'), 'parent_navigation_id');
			}
		}
		
		if ($this->get('callback_class') || $this->get('callback_method'))
		{
			$class = $this->get('callback_class');
			$method = $this->get('callback_method');

			if (!XenForo_Application::autoload($class) || !method_exists($class, $method))
			{
				$this->error(new XenForo_Phrase('please_enter_valid_callback_method'), 'callback_method');
			}
		}
	}

	/**
	 * Post-save handling.
	 */
	protected function _postSave()
	{
		if ($this->isUpdate() && $this->isChanged('navigation_id'))
		{
			$this->_renameMasterPhrase(
				$this->_getTitlePhraseName($this->getExisting('navigation_id')),
				$this->_getTitlePhraseName($this->get('navigation_id'))
			);

			$this->_db->update('ap_public_navigation',
				array('parent_navigation_id' => $this->get('navigation_id')),
				'parent_navigation_id = ' . $this->_db->quote($this->getExisting('navigation_id'))
			);

			$this->_renameMasterTemplate(
				$this->_getTemplateName($this->getExisting('navigation_id')),
				$this->_getTemplateName($this->get('navigation_id'))
			);
		}

		$titlePhrase = $this->getExtraData(self::DATA_TITLE);
		if ($titlePhrase !== null)
		{
			// If this is a primary link we need the phrase to be cached since we
			// are ourselves inserting the pre-processed tabs into the simpleCache.
			if($this->get('parent_navigation_id') == '')
				$this->_insertOrUpdateMasterPhraseCached(
					$this->_getTitlePhraseName($this->get('navigation_id')), $titlePhrase, 'NavManager'
				);
			else
				$this->_insertOrUpdateMasterPhrase(
					$this->_getTitlePhraseName($this->get('navigation_id')), $titlePhrase, 'NavManager'
				);
		}
		
		$parentNavigationId = $this->get('parent_navigation_id');
		$parentNavigationIdExisting = $this->getExisting('parent_navigation_id');
		if($parentNavigationId != '')
		{
			$this->_updateParentTemplate($parentNavigationId);
		}
		if($parentNavigationId != $parentNavigationIdExisting && $parentNavigationIdExisting != '')
		{
			$this->_updateParentTemplate($parentNavigationIdExisting);
		}
		
		XenForo_Application::setSimpleCacheData('publicTabs', $this->_getPublicNavigationModel()->preparePublicNavigationForDisplay());
	}

	/**
	 * Post-delete handling.
	 */
	protected function _postDelete()
	{
		$navigationId = $this->get('navigation_id');
		$parentNavigationId = $this->get('parent_navigation_id');

		$this->_deleteMasterPhrase($this->_getTitlePhraseName($navigationId));
		if($parentNavigationId == '')
		{
			$this->_deleteMasterTemplate($this->_getTemplateName($parentNavigationId));
		}
		else
		{
			$this->_updateParentTemplate($parentNavigationId);
		}

		$children = $this->_getPublicNavigationModel()->getPublicNavigationEntriesWithParent($navigationId);
		foreach ($children AS $child)
		{
			$dw = XenForo_DataWriter::create('NavManager_DataWriter_PublicNavigation');
			$dw->setExistingData($child, true);
			$dw->delete();
		}
		
		XenForo_Application::setSimpleCacheData('publicTabs', $this->_getPublicNavigationModel()->preparePublicNavigationForDisplay());
	}
	
	/**
	 * Inserts or updates a master (language 0) phrase and makes it cached.
	 * Errors will be silently ignored.
	 * 
	 * @param string $title
	 * @param string $text
	 * @param string $addonId
	 */
	protected function _insertOrUpdateMasterPhraseCached($title, $text, $addonId)
	{
		$this->_getPhraseModel()->insertOrUpdateMasterPhraseCached($title, $text, $addonId);
	}
	
	/**
	 * Renames a master template. If you get a conflict, it will
	 * be silently ignored.
	 *
	 * @param string $oldName
	 * @param string $newName
	 */
	protected function _renameMasterTemplate($oldName, $newName)
	{
		$this->_getTemplateModel()->renameMasterTemplate($oldName, $newName);
	}
	
	/**
	 * Inserts or updates a master template. Errors will be silently ignored.
	 *
	 * @param string $title
	 * @param string $text
	 * @param string $addOnId
	 */
	protected function _insertOrUpdateMasterTemplate($title, $text, $addOnId)
	{
		$this->_getTemplateModel()->insertOrUpdateMasterTemplate($title, $text, $addOnId);
	}
	
	/**
	 * Deletes a master template if it exists.
	 *
	 * @param string $title
	 */
	protected function _deleteMasterTemplate($title)
	{
		$this->_getTemplateModel()->deleteMasterTemplate($title);
	}

	/**
	 * Gets the name of the template for this navigation entry.
	 * This will report back a name even if the navigation entry doesn't own a template,
	 * i.e. it's a sublink.
	 * 
	 * @param string $navigationId
	 * 
	 * @return string
	 */
	protected function _getTemplateName($navigationId)
	{
		return $this->_getPublicNavigationModel()->getPublicNavigationTemplateName($navigationId);
	}
	
	/**
	 * Updates a specific parent template, or deletes it if it has no children
	 * 
	 * @param string $navigationId
	 */
	protected function _updateParentTemplate($navigationId)
	{
		$template = $this->_getPublicNavigationModel()->getLinksTemplate($navigationId);
		if ($template != null)
			$this->_insertOrUpdateMasterTemplate($this->_getTemplateName($navigationId), $template, 'NavManager');
		else
			$this->_deleteMasterTemplate($this->_getTemplateName($navigationId));
	}
	
	/**
	 * Gets the name of the title phrase for this navigation entry.
	 *
	 * @param string $navigationId
	 *
	 * @return string
	 */
	protected function _getTitlePhraseName($navigationId)
	{
		return $this->_getPublicNavigationModel()->getPublicNavigationPhraseName($navigationId);
	}
	
	/**
	 * Gets a properly formatted xen:link for a template
	 * 
	 * @param string $link
	 * @param string $data
	 * @param string $extraParams
	 * 
	 * @return string
	 */
	protected function _getXenLink($link, $data, $extraParams)
	{
		return $this->_getPublicNavigationModel()->getXenLink($link, $data, $extraParams);
	}

	/**
	 * @return NavManager_Model_PublicNavigation
	 */
	protected function _getPublicNavigationModel()
	{
		return $this->getModelFromCache('NavManager_Model_PublicNavigation');
	}
	
	/**
	 * @return XenForo_Model_Template
	 */
	protected function _getTemplateModel()
	{
		return $this->getModelFromCache('XenForo_Model_Template');
	}
}