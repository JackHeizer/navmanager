<?php

/**
 * Helper to get the public navigation node export data (likely in XML format).
 *
 * @package NavManager_PublicNavigation
 */
class NavManager_ViewAdmin_PublicNavigation_ExportMultiple extends XenForo_ViewAdmin_Base
{
	/**
	 * Render the exported data to XML.
	 *
	 * @return string
	 */
	public function renderXml()
	{
		$this->setDownloadFileName('publicNavigation-' . $this->_params['navigation_id'] . '.xml');
		return $this->_params['xml']->saveXml();
	}
}