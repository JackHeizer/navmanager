<?php

class NavManager_Listener_PublicNavigation
{	
	public static function template_hook($hookName, &$contents, array $hookParams, XenForo_Template_Abstract $template)
	{
		if ($hookName == 'header')
		{
			$contents = $template->create('navmanager_header', $template->getParams());
			
		}
	}
	
	public static function navigation_tabs(array &$extraTabs, $selectedTabId)
	{	
		$tabs = XenForo_Application::getSimpleCacheData('publicTabs');
		if($tabs === false)
		{
			$navigationModel = XenForo_Model::create('NavManager_Model_PublicNavigation');
			$tabs = $navigationModel->preparePublicNavigationForDisplay();
			XenForo_Application::setSimpleCacheData('publicTabs', $tabs);
		}
		
		foreach ($tabs AS $tabKey => $tabValue)
		{
			// If the visitor doesn't have the proper permission, skip it and all of its children
			if(!NavManager_Permission::hasPermission($tabValue['permission_group_id'], $tabValue['permission_id']))
				continue;
			$extraTabs[$tabKey]['title'] = $tabValue['title'];
			$extraTabs[$tabKey]['href'] = $tabValue['href'];
			$extraTabs[$tabKey]['position'] = $tabValue['position'];
			if(!empty($tabValue['linksTemplate']))
				$extraTabs[$tabKey]['linksTemplate'] = $tabValue['linksTemplate'];
			if(!empty($tabValue['children']))
			{
				foreach($tabValue['children'] AS $child)
				{
					// Add permission variables for combo permissions
					$permission = NavManager_Permission::getPermissionParam($child['permission_group_id'], $child['permission_id']);
					if($permission != null)
					{
						// If it's not null it's a combo permission and the variable should be
						// the permission id. I should find a way to make this extensible...
						$extraTabs[$child['parent_navigation_id']][$child['permission_id']] = $permission;
					}
					if(!empty($child['callback_class']) && !empty($child['callback_method']))
					{
						$navTab = array(&$extraTabs[$child['parent_navigation_id']]);
						call_user_func(array($child['callback_class'], $child['callback_method']), &$navTab);
					}
				}
			}
			if(!empty($tabValue['callback_class']) && !empty($tabValue['callback_method']))
			{
				$navTab = array(&$extraTabs[$tabKey]);
				call_user_func(array($tabValue['callback_class'], $tabValue['callback_method']), &$navTabs);
			}
		}
	}
	
	public static function load_class_model($class, array &$extend)
	{
		switch($class)
		{
			case 'XenForo_Model_Template':
				$extend[] = 'NavManager_Model_Template';
				break;
			case 'XenForo_Model_Phrase':
				$extend[] = 'NavManager_Model_Phrase';
				break;
		}		
	}
}