<?php

/**
 * NavManager special permission accessor helper.
 *
 * @package NavManager_Permissions
 */
class NavManager_Permission
{
	/**
	 * Helpder function to get permissions for a navigation node
	 * 
	 * @param string $permissionGroupId
	 * @param string $permissionId
	 * 
	 * @return boolean
	 */
	public static function hasPermission($permissionGroupId, $permissionId)
	{
		// If either are empty then there are no permissions
		// associated with this node, do the opposite of what
		// XenForo does and return true
		if(empty($permissionGroupId) || empty($permissionId))
			return true;
		$visitor = XenForo_Visitor::getInstance();
		if($permissionGroupId == 'nmSpecialPermissions')
		{
			switch($permissionId)
			{
				case 'loggedIn':
					return $visitor->getUserId();
				case 'canSearch':
					return $visitor->canSearch();
				case 'canUploadAvatar':
					return $visitor->canUploadAvatar();
				case 'canEditSignature':
					return $visitor->canEditSignature();
				case 'canUpdateStatus':
					return $visitor->canUpdateStatus();
			}
		}
		else
		{
			return $visitor->hasPermission($permissionGroupId, $permissionId);
		}
	}
	
	/**
	 * Gets the permission parameter for a secondary navigation node
	 * This could probably be generalized to handle custom combo
	 * permissions, but not now...
	 * 
	 * @param string $permissionGroupId
	 * @param string $permissionId
	 * 
	 * @return boolean
	 */
	public static function getPermissionParam($permissionGroupId, $permissionId)
	{
		if(empty($permissionGroupId) || empty($permissionId))
			return null;
		$visitor = XenForo_Visitor::getInstance();
		if($permissionGroupId == 'nmSpecialPermissions')
		{
			switch($permissionId)
			{
				case 'loggedIn':
					// {$visitor.user_id works just fine}
					return null;
				case 'canSearch':
					return $visitor->canSearch();
				case 'canUploadAvatar':
					return $visitor->canUploadAvatar();
				case 'canEditSignature':
					return $visitor->canEditSignature();
				case 'canUpdateStatus':
					return $visitor->canUpdateStatus();
			}
		}
		else
		{
			return null;
		}
	}
}