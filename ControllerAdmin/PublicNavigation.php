<?php

/**
 * Controller for public navigation tasks the admin control panel.
 *
 * @package NavManager_PublicNavigation
 */
class NavManager_ControllerAdmin_PublicNavigation extends XenForo_ControllerAdmin_Abstract
{
	/**
	 * I should create a custom permission for this, someone remind me later
	 * 
	 * @see XenForo_Controller::_preDispatch()
	 */
	protected function _preDispatch($action)
	{
		$this->assertAdminPermission('publicNavigation');
	}
	

	/**
	 * Display a tree of public navigation entries.
	 *
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionIndex()
	{
		$navigationModel = $this->_getPublicNavigationModel();

		$viewParams = array(
			'publicNavigation' => $navigationModel->preparePublicNavigationEntries($navigationModel->getPublicNavigationInOrder())
		);

		return $this->responseView('NavManager_ViewAdmin_PublicNavigation_List', 'public_navigation_list', $viewParams);
	}

	/**
	 * Gets the controller response for adding/editing a public navigation entry.
	 *
	 * @param array $navigation
	 *
	 * @return XenForo_ControllerResponse_Abstract
	 */
	protected function _getNavigationAddEditResponse(array $navigation)
	{
		$navigationModel = $this->_getPublicNavigationModel();
		$permissionModel = $this->_getPermissionModel();
		
		$interfaceGroups = $permissionModel->preparePermissionInterfaceGroups($permissionModel->getAllPermissionInterfaceGroups());
		$interfaceGroups = array('nmSpecialPermissions' => array(
			'interface_group_id' => 'nmSpecialPermissions',
			'title' => new XenForo_Phrase('public_navigation_special_perm_iface_title')
		)) + $interfaceGroups;
		$permissions = $permissionModel->preparePermissions($permissionModel->getAllPermissions());
		$permissionsGrouped = array();
		$permissionsGrouped['nmSpecialPermissions'][] = array(
			'permission_id' => 'loggedIn',
			'permission_group_id' => 'nmSpecialPermissions',
			'interface_group_id' => 'nmSpecialPermissions',
			'title' => new XenForo_Phrase('public_navigation_special_perm_logged_in_title')
		);
		$permissionsGrouped['nmSpecialPermissions'][] = array(
			'permission_id' => 'canSearch',
			'permission_group_id' => 'nmSpecialPermissions',
			'interface_group_id' => 'nmSpecialPermissions',
			'title' => new XenForo_Phrase('public_navigation_special_perm_can_search_title')
		);
		$permissionsGrouped['nmSpecialPermissions'][] = array(
			'permission_id' => 'canUploadAvatar',
			'permission_group_id' => 'nmSpecialPermissions',
			'interface_group_id' => 'nmSpecialPermissions',
			'title' => new XenForo_Phrase('public_navigation_special_perm_avatar_title')
		);
		$permissionsGrouped['nmSpecialPermissions'][] = array(
			'permission_id' => 'canEditSignature',
			'permission_group_id' => 'nmSpecialPermissions',
			'interface_group_id' => 'nmSpecialPermissions',
			'title' => new XenForo_Phrase('public_navigation_special_perm_signature_title')
		);
		$permissionsGrouped['nmSpecialPermissions'][] = array(
			'permission_id' => 'canUpdateStatus',
			'permission_group_id' => 'nmSpecialPermissions',
			'interface_group_id' => 'nmSpecialPermissions',
			'title' => new XenForo_Phrase('public_navigation_special_perm_status_title')
		);
		foreach ($permissions AS $permission)
		{
			if (isset($interfaceGroups[$permission['interface_group_id']]) && $permission['permission_type'] == 'flag')
			{
				$permissionsGrouped[$permission['interface_group_id']][] = $permission;
			}
		}
		
		$viewParams = array(
			'publicNavigation' => $navigation,
			'navigationOptions' => $navigationModel->getPublicNavigationOptions(),
			'masterTitle' => $navigationModel->getPublicNavigationMasterTitlePhraseValue($navigation['navigation_id']),
			'interfaceGroups' => $interfaceGroups,
			'permissionsGrouped' => $permissionsGrouped,
		);

		return $this->responseView('NavManager_ViewAdmin_PublicNavigation_Edit', 'public_navigation_edit', $viewParams);
	}

	/**
	 * Displays a form to add a public navigation entry.
	 *
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionAdd()
	{
		$navigation = array(
			'navigation_id' => '',
			'parent_navigation_id' => $this->_input->filterSingle('parent', XenForo_Input::STRING),
			'display_order' => 10,
			'intern' => 1
		);
		return $this->_getNavigationAddEditResponse($navigation);
	}

	/**
	 * Displays a form to edit a public navigation entry.
	 *
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionEdit()
	{
		$navigationId = $this->_input->filterSingle('navigation_id', XenForo_Input::STRING);
		$navigation = $this->_getPublicNavigationOrError($navigationId);
		$navigation['permission_group_id_id'] = $navigation['permission_group_id'] . '.' . $navigation['permission_id'];

		return $this->_getNavigationAddEditResponse($navigation);
	}

	/**
	 * Updates or inserts a public navigation entry.
	 *
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionSave()
	{
		$this->_assertPostOnly();

		$navigationId = $this->_input->filterSingle('navigation_id', XenForo_Input::STRING);
		$newNavigationId = $this->_input->filterSingle('new_navigation_id', XenForo_Input::STRING);
		$dwInput = $this->_input->filter(array(
			'parent_navigation_id' => XenForo_Input::STRING,
			'display_order' => XenForo_Input::UINT,
			'link' => XenForo_Input::STRING,
			'data' => XenForo_Input::STRING,
			'extra_params' => XenForo_Input::STRING,
			'overlay' => XenForo_Input::UINT,
			'full' => XenForo_Input::UINT,
			'intern' => XenForo_Input::UINT,
			'callback_class' => XenForo_Input::STRING,
			'callback_method' => XenForo_Input::STRING
		));
		$permissionGroupIdId = $this->_input->filterSingle('permission_group_id_id', XenForo_Input::STRING);
		if($permissionGroupIdId == '')
		{
			$permissionGroupId = '';
			$permissionId = '';
		}
		else
			list($permissionGroupId, $permissionId) = explode('.', $permissionGroupIdId);
		
		$titlePhrase = $this->_input->filterSingle('title', XenForo_Input::STRING);

		$dw = XenForo_DataWriter::create('NavManager_DataWriter_PublicNavigation');
		if ($navigationId)
		{
			$dw->setExistingData($navigationId);
		}
		$dw->set('navigation_id', $newNavigationId);
		$dw->set('permission_id', $permissionId);
		$dw->set('permission_group_id', $permissionGroupId);
		$dw->bulkSet($dwInput);
		$dw->setExtraData(NavManager_DataWriter_PublicNavigation::DATA_TITLE, $titlePhrase);
		$dw->save();

		return $this->responseRedirect(
			XenForo_ControllerResponse_Redirect::SUCCESS,
			XenForo_Link::buildAdminLink('public-navigation') . $this->getLastHash($newNavigationId)
		);
	}

	/**
	 * Deletes a public navigation entry.
	 *
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionDelete()
	{
		if ($this->isConfirmedPost())
		{
			return $this->_deleteData(
				'NavManager_DataWriter_PublicNavigation', 'navigation_id',
				XenForo_Link::buildAdminLink('public-navigation')
			);
		}
		else // show confirmation dialog
		{
			$navigationId = $this->_input->filterSingle('navigation_id', XenForo_Input::STRING);
			$navigation = $this->_getPublicNavigationOrError($navigationId);

			$dw = XenForo_DataWriter::create('NavManager_DataWriter_PublicNavigation', XenForo_DataWriter::ERROR_EXCEPTION);
			$dw->setExistingData($navigation, true);
			$dw->preDelete();

			$viewParams = array(
				'publicNavigation' => $navigation
			);

			return $this->responseView(
				'NavManager_ViewAdmin_PublicNavigation_Delete',
				'public_navigation_delete', $viewParams
			);
		}
	}
	
	/**
	 * Displays a form to let a user choose what public navigation tabs (in an XML file) to import
	 * 
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionImportConfirm()
	{
		return $this->responseView('NavManager_ViewAdmin_PublicNavigation_Import', 'public_navigation_import');
	}
	
	/**
	 * Imports a navigation node XML file
	 * 
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionImport()
	{
		$this->_assertPostOnly();

		$fileTransfer = new Zend_File_Transfer_Adapter_Http();
		if ($fileTransfer->isUploaded('upload_file'))
		{
			$fileInfo = $fileTransfer->getFileInfo('upload_file');
			$fileName = $fileInfo['upload_file']['tmp_name'];
		}
		
		$overwrite = $this->_input->filterSingle('overwrite', XenForo_Input::UINT);
		
		$this->_getPublicNavigationModel()->importPublicNavigationNodeXML($fileName, $overwrite);
		
		return $this->responseRedirect(XenForo_ControllerResponse_Redirect::SUCCESS, XenForo_Link::buildAdminLink('public-navigation'));
	}
	
	/**
	 * Exports an public navigation node's XML data.
	 *
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionExport()
	{
		$navigationId = $this->_input->filterSingle('navigation_id', XenForo_Input::STRING);

		$this->_routeMatch->setResponseType('xml');

		$viewParams = array(
			'minor_title' => $navigationId,
			'xml' => $this->_getPublicNavigationModel()->getPublicNavigationNodeXML($navigationId)
		);

		return $this->responseView('NavManager_ViewAdmin_PublicNavigation_Export', '', $viewParams);
	}
	
	/**
	 * Displays a form to let a user select which tabs to export and what the file should be called
	 * 
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionExportMultipleConfirm()
	{
		$navigationModel = $this->_getPublicNavigationModel();

		$viewParams = array(
			'publicNavigation' => $navigationModel->preparePublicNavigationEntries($navigationModel->getPublicNavigationInOrder())
		);
		
		return $this->responseView('NavManager_ViewAdmin_PublicNavigation_ExportMultiple', 'public_navigation_export', $viewParams);
	}
	
	/**
	 * Exports multiple public navigation nodes to an XML file
	 * 
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionExportMultiple()
	{
		$this->_assertPostOnly();
		
		$export = $this->_input->filterSingle('export', array(XenForo_Input::UINT, 'array' => true));
		
		$this->_routeMatch->setResponseType('xml');
		
		$viewParams = array(
			'minor_title' => 'NodeList',
			'xml' => $this->_getPublicNavigationModel()->getPublicNavigationNodesXML(array_keys($export))
		);
		
		return $this->responseView('NavManager_ViewAdmin_PublicNavigation_Export', '', $viewParams);
	}

	/**
	 * Gets the specified public navigation entry or errors.
	 *
	 * @param string $id
	 *
	 * @return array
	 */
	protected function _getPublicNavigationOrError($id)
	{
		$info = $this->_getPublicNavigationModel()->getPublicNavigationEntryById($id);
		if (!$info)
		{
			throw $this->responseException($this->responseError(new XenForo_Phrase('requested_public_navigation_entry_not_found'), 404));
		}

		return $this->_getPublicNavigationModel()->preparePublicNavigationEntry($info);
	}

	/**
	 * @return NavManager_Model_PublicNavigation
	 */
	protected function _getPublicNavigationModel()
	{
		return $this->getModelFromCache('NavManager_Model_PublicNavigation');
	}
	
	/**
	 * Gets the permission model.
	 *
	 * @return XenForo_Model_Permission
	 */
	protected function _getPermissionModel()
	{
		return $this->getModelFromCache('XenForo_Model_Permission');
	}
}