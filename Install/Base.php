<?php

class NavManager_Install_Base
{
	private static $_instance;

	public static final function getInstance()
	{
		if (!self::$_instance)
		{
			self::$_instance = new self;
		}

		return self::$_instance;
	}
	
	public static function install($existingAddOn, $addOnData)
	{
		$start = 1;
		$end = $addOnData['version_id'];

		if ($existingAddOn)
		{
			$start = $existingAddOn['version_id'];
		}
		
		$base = self::getInstance();

		for ($i = $start; $i <= $end; $i++)
		{
			$method = '_installStep' . $i;

			if (method_exists($base, $method) === false)
			{
				continue;
			}

			$base->$method();
		}
	}
	
	protected function _installStep1()
	{
		$db = XenForo_Application::get('db');
		
		XenForo_Db::beginTransaction($db);
		
		$db->query("
			CREATE TABLE IF NOT EXISTS ap_public_navigation (
				navigation_id VARCHAR(25) NOT NULL,
				parent_navigation_id VARCHAR(25) NOT NULL,
				display_order INT(10) UNSIGNED NOT NULL DEFAULT 10,
				perm VARCHAR(150) DEFAULT '',
				link VARCHAR(150) NOT NULL DEFAULT '',
				data VARCHAR(150) DEFAULT '',
				extra_params VARCHAR(150) DEFAULT '',
				overlay TINYINT(3) UNSIGNED DEFAULT 0,
				full TINYINT(3) UNSIGNED DEFAULT 0,
				intern TINYINT(3) UNSIGNED DEFAULT 1,
				callback_class VARCHAR(75) DEFAULT '',
				callback_method VARCHAR(75) DEFAULT '',
				PRIMARY KEY (navigation_id),
				KEY parent_navigation_id_display_order (parent_navigation_id, display_order)
			) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci
		");
		
		$homeUrl = XenForo_Application::get('options')->homePageUrl;

		$db->query("
			INSERT INTO ap_public_navigation
				(navigation_id, parent_navigation_id, display_order, perm, link, data, extra_params, overlay, full, intern, callback_class, callback_method)
			VALUES
				('home', '', 10, '', " . $db->quote($homeUrl) . ", '', '', 0, 0, 0, '', ''),
				('forums', '', 20, '', 'index', '', '', 0, 1, 1, '', ''),
				('markAllForumsRead', 'forums', 10, '{\$visitor.user_id}', 'forums/-/mark-read', '', 'date={\$serverTime}', 1, 0, 1, '', ''),
				('searchForums', 'forums', 20, '{\$canSearch}', 'search', '', 'type=post', 0, 0, 1, 'NavManager_Callback_PublicNavigation', 'canSearch'),
				('watchedThreads', 'forums', 30, '{\$visitor.user_id}', 'watched/threads', '', '', 0, 0, 1, '', ''),
				('whatsNew', 'forums', 40, '', 'find-new/threads', '', '', 0, 0, 1, '', ''),
				('members', '', 30, '', 'members', '', '', 0, 1, 1, '', ''),
				('registeredMembers', 'members', 10, '', 'members', '', '', 0, 0, 1, '', ''),
				('currentVisitors', 'members', 20, '', 'online', '', '', 0, 0, 1, '', ''),
				('recentActivity', 'members', 30, '', 'recent-activity', '', '', 0, 0, 1, '', ''),
				('help', '', 40, '', 'help', '', '', 0, 1, 1, '', ''),
				('smilies', 'help', 10, '', 'help/smilies', '', '', 0, 0, 1, '', ''),
				('bbCodes', 'help', 20, '', 'help/bb-codes', '', '', 0, 0, 1, '', ''),
				('trophies', 'help', 30, '', 'help/trophies', '', '', 0, 0, 1, '', '')
		");
		
		$publicNavigationModel = XenForo_Model::create('NavManager_Model_PublicNavigation');
		$phraseModel = XenForo_Model::create('XenForo_Model_Phrase');
		$phrases = array(
			'home' => 'Home',
			'forums' => 'Forums',
			'markAllForumsRead' => 'Mark All Forums Read',
			'searchForums' => 'Search Forums',
			'watchedThreads' => 'Watched Threads',
			'whatsNew' => 'What\'s New?',
			'members' => 'Members',
			'registeredMembers' => 'Registered Members',
			'currentVisitors' => 'Current Visitors',
			'recentActivity' => 'Recent Activity',
			'help' => 'Help',
			'smilies' => 'Smilies',
			'bbCodes' => 'BB Codes',
			'trophies' => 'Trophies'
		);
		
		foreach ($phrases AS $phraseKey => $phraseValue)
		{
			$phraseModel->insertOrUpdateMasterPhrase(
				$publicNavigationModel->getPublicNavigationPhraseName($phraseKey),
				$phraseValue,
				'NavManager'
			);
		}
		
		XenForo_Db::commit($db);
	}
	
	protected function _installStep2()
	{
		$metaLinks = '<ul class="secondaryContent blockLinksList">{$links}</ul>';
		$metaLink = '<li><a href="{$link}" {$overlay}>{$title}</a></li>';
		$metaPerm = '<xen:if is="{$perm}">{$permLink}</xen:if>';
		
		$db = XenForo_Application::get('db');
		XenForo_Db::beginTransaction($db);
		
		$publicNavigationModel = XenForo_Model::create('NavManager_Model_PublicNavigation');
		$templateModel = XenForo_Model::create('XenForo_Model_Template');
		
		$nodes = $publicNavigationModel->getPublicNavigationEntriesWithParent('');
		
		foreach ($nodes AS $nodeKey => $nodeValue)
		{	
			$dw = XenForo_DataWriter::create('XenForo_DataWriter_Template', XenForo_DataWriter::ERROR_EXCEPTION);
			
			$templateText = $publicNavigationModel->getLinksTemplate($nodeKey, $metaLinks, $metaLink, $metaPerm);
			if($templateText === null)
				continue;
				
			$title = $publicNavigationModel->getPublicNavigationTemplateName($nodeKey);
			
			$template = $templateModel->getTemplateInStyleByTitle($title, 0);

			if ($template)
			{
				$dw->setExistingData($template, true);
			}
			$dw->setOption(XenForo_DataWriter_Template::OPTION_DEV_OUTPUT_DIR, 'test_dir');
			$dw->setOption(XenForo_DataWriter_Template::OPTION_FULL_COMPILE, false);
			$dw->setOption(XenForo_DataWriter_Template::OPTION_TEST_COMPILE, false);
			$dw->setOption(XenForo_DataWriter_Template::OPTION_CHECK_DUPLICATE, false);
			$dw->setOption(XenForo_DataWriter_Template::OPTION_REBUILD_TEMPLATE_MAP, false);
			$dw->bulkSet(array(
				'style_id' => 0,
				'title' => $title,
				'template' => $templateText,
				'addon_id' => 'NavManager'
			));
			$dw->save();
		}
		
		XenForo_Db::commit($db);
	}
	
	public static function uninstall()
	{
		$end = 2;
		
		$base = self::getInstance();
		
		for ($i = 1; $i <= $end; $i++)
		{
			$method = '_uninstallStep' . $i;

			if (method_exists($base, $method) === false)
			{
				continue;
			}

			$base->$method();
		}
	}
	
	protected function _uninstallStep1()
	{
		$db = XenForo_Application::get('db');;
		
		$db->query("
			DROP TABLE IF EXISTS ap_public_navigation
		");
	}
}